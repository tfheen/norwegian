Thesaurus for Bokm�l and Nynorsk
--------------------------------
  Petter Reinholdtsen, 2005-08-05

To use the thesaurus files on Debian, the programs thescoder is
needed.  Generate and install the files using thescoder, and update
the dicts file to register the new thesaurus with openoffice.

It would be nice if update-openoffice-dicts (from dictionaries-common)
were able to update the dicts file automatically.

The file format of the data files is simple.  The first line is the
set of of allowed characters.  The next lines are semicolon separated
words and their synonyms.
