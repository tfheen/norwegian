norwegian (2.2-4) unstable; urgency=medium

  * Fix invocation of dh_auto_build to properly pass the right args.
    Thanks to Chris Lamb for the patch.  Closes: #900290
  * Bump debhelper compat to 11.
  * Ship nb.cwl and nn.cwl files to make aspell/aspell-autobuildhash
    happier.

 -- Tollef Fog Heen <tfheen@debian.org>  Thu, 07 Jun 2018 21:43:54 +0200

norwegian (2.2-3) unstable; urgency=medium

  * Fix up where the no{.{dat,multi},_phonet.dat} symlinks point.
    Closes: #857781

 -- Tollef Fog Heen <tfheen@debian.org>  Thu, 20 Jul 2017 19:57:07 +0200

norwegian (2.2-2) unstable; urgency=medium

  * Turn off parallel builds.  Closes: #844142

 -- Tollef Fog Heen <tfheen@debian.org>  Sun, 13 Nov 2016 20:15:00 +0100

norwegian (2.2-1) unstable; urgency=medium

  * New upstream version.
    - Fixes FTBFS.  Closes: #790765
    - Add missing scripts/thes_to_dat script from git.
    - Add libmythes-dev
  * Add Brazilian Portuguese debconf translation.  Closes: #811513
  * Add Homepage field to control file.  Closes: #821373
  * Always use gawk as the awk.  Closes: #802950
  * Bump to debhelper 10.
  * Install hunspell files in hunspell, drop installing myspell links.
  * Fix formatting of Build-Depends.
  * Make aspell-no Arch: all
  * Bump Standards-Version to 3.9.8.

 -- Tollef Fog Heen <tfheen@debian.org>  Wed, 26 Oct 2016 21:54:36 +0200

norwegian (2.0.10-7) unstable; urgency=medium

  * Fix FTBFS by moving iconv-ing the word lists from override_dh_install
    to binary-indep.  Closes: #784974

 -- Tollef Fog Heen <tfheen@debian.org>  Sat, 30 May 2015 12:11:17 +0200

norwegian (2.0.10-6) unstable; urgency=medium

  * Fix build problem with nynorsk dictionaries.  Thanks to Petter
    Reinholdtsen for patch. Closes: #777371
  * Fix encoding of, and unfuzzy English Debconf translation.  Closes: #691979
  * Add Polish Debconf translation.  Closes: #734106
  * Correct name of New Norwegian and bokmal → bokmål in
    wnorwegian.info-wordlist.
  * Re-encode word lists in UTF-8.  Closes: #709011

 -- Tollef Fog Heen <tfheen@debian.org>  Fri, 08 May 2015 20:59:50 +0200

norwegian (2.0.10-5.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Rebuild with aspell supporting semi-multiarch and make sure to not use
    old aspell (Closes: #763908).

 -- Agustin Martin Domingo <agmartin@debian.org>  Mon, 20 Oct 2014 11:11:38 +0200

norwegian (2.0.10-5.1) unstable; urgency=low

  * Non-maintainer upload, acked by the maintainer.
  * Compress binaries with xz to save some space on CD images.  Closes: #688676

 -- Cyril Brulebois <kibi@debian.org>  Tue, 25 Sep 2012 21:09:32 +0200

norwegian (2.0.10-5) unstable; urgency=low

  * Switch to dh, update build-depends accordingly.
  * Update debhelper compat level to 8.

 -- Tollef Fog Heen <tfheen@debian.org>  Sun, 06 Nov 2011 15:57:35 +0100

norwegian (2.0.10-4) unstable; urgency=low

  * Ack NMU.  Closes: #547462
  * Rebuild with newer ispell.  Closes: #623073
  * Drop iso8859-1 symlinks.
  * Update Danish debconf translation.  Closes: #596144

 -- Tollef Fog Heen <tfheen@debian.org>  Sat, 08 Aug 2009 22:16:22 +0200

norwegian (2.0.10-3.2) unstable; urgency=low

  * fix debian/myspell-nb.links, oops 

 -- Rene Engelhard <rene@debian.org>  Sun, 20 Sep 2009 22:50:50 +0200

norwegian (2.0.10-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * move myspell dicts to /usr/share/hunspell and add compat symlinks
    from /usr/share/myspell/dicts (closes: #541938)
  * move hyphenation patterns to /usr/share/hyphen and add compat symlinks
    from /usr/share/myspell/dicts (closes: #541898)
  * reinstate n{b,n}.{dic,aff} as symlinks as Mozilla needs it.

 -- Rene Engelhard <rene@debian.org>  Sun, 20 Sep 2009 01:00:34 +0200

norwegian (2.0.10-3) unstable; urgency=low

  * Fix up URL in copyright
  * Add Japanese debconf template.  Closes: #512862
  * Rename n[bn].{aff,dic} to n[bn]_NO.{aff,dic}.  Closes: #517783, #517784
  * Add ${misc:Depends} to Depends to shut up lintian a bit.
  * Remove explicit coding of changelog at the end, all changelogs should
    be UTF8 now.
  * Bump debhelper compat version to 6.
  * Bump Standards-Version to 3.8.1 (no changes needed).
  
 -- Tollef Fog Heen <tfheen@debian.org>  Sun, 22 Mar 2009 16:09:37 +0100

norwegian (2.0.10-2) unstable; urgency=low

  * Fix up confusion in debian/myspell-n[bn].info-myspell which broke
    hyphenation support in openoffice.org.  Closes: 483806
  * Correct typo in aspell-no.postinst (missing space before ]).

 -- Tollef Fog Heen <tfheen@debian.org>  Fri, 20 Jun 2008 21:51:04 +0200

norwegian (2.0.10-1) unstable; urgency=low

  * New upstream release.  Closes: #473531
  * Revert "* [Lintian] Fix debian/changelog (emacs variables removed)"
    from NMU.
  * Update myspell-n[bn].info.myspell to make OOo able to use the
    Norwegian spellchecker.  Also add hyphenation patterns.  Thanks to
    Petter Reinhodtsen for the fix.
  * Drop ssed build dependency since GNU sed is now fixed to not be
    disastrously slow.

 -- Tollef Fog Heen <tfheen@debian.org>  Sun, 06 Apr 2008 11:10:55 +0200

norwegian (2.0.9-2.1) unstable; urgency=low

  * Non-maintainer upload to fix pending l10n issues.
  * Debconf templates and debian/control reviewed by the debian-l10n-
    english team as part of the Smith review project. Closes: #433066
  * Debconf translation updates:
    - Basque. Closes: #433766
    - Swedish. Closes: #433772
    - Vietnamese. Closes: #433797
    - Russian. Closes: #433823
    - German. Closes: #433839
    - Czech. Closes: #434005
    - Italian. Closes: #434269
    - French. Closes: #434324
    - Galician. Closes: #434314
    - Spanish. Closes: #434868
    - Dutch. Closes: #435327
    - Portuguese. Closes: #435500
    - Norwegian Bokmål.
    - Norwegian Nynorsk.
    - Finnish. Closes: #457425
  * [Lintian] Correct FSF address in debian/copyright
  * [Lintian] Fix debian/changelog (emacs variables removed)
  * [Lintian] Fix extended descriptions for the myspell-* packages
              (too long lines)
  * [Lintian] Remove extra (invalid) PO file debian/po/sv2.po
  * [Lintian] No longer ignore error by "make clean"

 -- Christian Perrier <bubulle@debian.org>  Sun, 17 Feb 2008 16:14:22 +0100
 
norwegian (2.0.9-1) unstable; urgency=low

  * New upstream release
  * Change build-depends from libmyspell-dev to hunspell-tools |
    myspell-tools. Closes: #426536
  * Get rid of /usr/lib/aspell-0.60/no.dat and friends if they exist.
    Closes: 382366.
  * Tweak the templates a bit to make translations easier to do.  
    Closes: #414137
  * Add Dutch translation.  Closes: #415498
  * Add Portuguese translation.  Closes: #416817
  * Install .dic and .aff files for myspell-{nb,nn} as {nb,nn}.{dic,aff},
    not {nb,nn}_NO.{dic,aff}.  Closes: #373746, #373749

 -- Tollef Fog Heen <tfheen@debian.org>  Sun, 17 Jun 2007 17:55:04 +0200

norwegian (2.0.8-2.1) unstable; urgency=low

  * Non-maintainer upload to fix pending l10n issues.
  * Debconf translations:
    - Spanish. Closes: #384317
    - Remove useless norwegian-cs.po file. Closes: #404129

 -- Christian Perrier <bubulle@debian.org>  Fri, 16 Feb 2007 23:25:12 +0100

norwegian (2.0.8-2) unstable; urgency=low

  * Fix symlinks so we have an UTF8 symlink pointing to nb.hash too.

 -- Tollef Fog Heen <tfheen@debian.org>  Tue, 13 Jun 2006 12:35:46 +0200

norwegian (2.0.8-1) unstable; urgency=low

  * New upstream release, (Closes: #350035)
  * Fix name of bokmål word list.  (Closes: #352666)
  * Remove symlinks created in postinst in prerm.  (Closes: #355688)

 -- Tollef Fog Heen <tfheen@debian.org>  Tue,  6 Jun 2006 13:16:34 +0200

norwegian (2.0-23) unstable; urgency=low

  * Make aspell-no provide aspell6a-dictionary and be arch: any 
    (closes: #340756)
  * Include Swedish debconf translation. (closes: #333318)

 -- Tollef Fog Heen <tfheen@debian.org>  Sun, 27 Nov 2005 10:31:53 +0100

norwegian (2.0-22) unstable; urgency=low

  * Depend on debconf | debconf-2.0

 -- Tollef Fog Heen <tfheen@debian.org>  Tue, 27 Sep 2005 12:15:13 +0200

norwegian (2.0-21) unstable; urgency=low

  * Update to newer aspell policy.  (Closes: #319665)

 -- Tollef Fog Heen <tfheen@debian.org>  Tue, 23 Aug 2005 12:19:04 +0200

norwegian (2.0-20) unstable; urgency=low

  * Update german debconf translation, thanks to Jens Seidel
    (Closes: #313802)
  * Add Vietnamese debconf translation, thanks to Clytie Siddall
    (Closes: #316224)
  * Add Czech debconf translation, thanks to Miroslav Kure
    (Closes: #287539)

 -- Tollef Fog Heen <tfheen@debian.org>  Mon, 18 Jul 2005 06:14:54 +0200

norwegian (2.0-19) unstable; urgency=high

  * Ack NMU (closes: 295007).
  * Fix broken Depends caused by NMU.  This caused aspell-no to propagate
    to testing before aspell 0.6 did, which broke.

 -- Tollef Fog Heen <tfheen@debian.org>  Fri,  4 Mar 2005 13:01:02 +0100

norwegian (2.0-18.1) unstable; urgency=low

  * Non-maintainer upload
  * Transitioned the aspell-no package to Aspell 0.60 (Closes: #295007)
    - debian/control: Build-depend on aspell-bin (>> 0.60)
    - debian/control: Provide aspell6-dictionary
    - debian/aspell-no.dirs: install /usr/lib/aspell-0.60 instead of
      /usr/share/aspell and /usr/lib/aspell, removed obsolete
      /usr/share/pspell
    - debian/rules: install aspell-no files into /usr/lib/aspell-0.60
    - debian/aspell-no.postinst: only muck around in /usr/lib/aspell-0.60

 -- Brian Nelson <pyro@debian.org>  Fri, 18 Feb 2005 23:36:08 -0800
   
norwegian (2.0-18) unstable; urgency=low

  * Clarify copyright file.  (closes: #290216)
  * Stop using frequency information, as it seems to be
    unreliable. (closes: #286542)
  * Provide bokmaal links as well (closes: #266030)
  * Add Czech debconf translations.
  
 -- Tollef Fog Heen <tfheen@debian.org>  Sat,  5 Feb 2005 15:58:18 +0100

norwegian (2.0-17) unstable; urgency=low

  * Fix upstream author email address. (closes: #254145)
  * Update German debconf translation, thanks to Helge Kreutzmann. 
    (closes: #251734) 

 -- Tollef Fog Heen <tfheen@debian.org>  Sat, 17 Jul 2004 13:20:38 +0200

norwegian (2.0-16) unstable; urgency=low

  * Fix typo in debian/inorwegian.templates.in (closes: #246413)
  * Add French debconf templates. (closes: #243641)

 -- Tollef Fog Heen <tfheen@debian.org>  Sun, 30 May 2004 15:53:51 +0200

norwegian (2.0-15) unstable; urgency=low

  * Add build-dep on libmyspell-dev (>= 1:3.1-5) (closes: #245318)

 -- Tollef Fog Heen <tfheen@debian.org>  Fri, 23 Apr 2004 09:42:07 +0200

norwegian (2.0-14) unstable; urgency=low

  * Provide myspell-nb and myspell-nn packages. (closes: #237972)
  * Update Danish debconf translation (closes: #241844)

 -- Tollef Fog Heen <tfheen@debian.org>  Sat, 10 Apr 2004 01:08:32 +0200

norwegian (2.0-13) unstable; urgency=low

  * Switch to po-debconf (closes: #232140, #217110)
  * Rebuild with updated build-deps. (closes: #232174, #232184)
  * Fix the multi files a bit more (closes: 235440)  (Thanks to Jerome
    Lacoste for the suggested fix.)
  * Fix tiny bug in generation of en.po
  * Convert changelog to UTF8.
  * Rename no.po to nb.po.

 -- Tollef Fog Heen <tfheen@debian.org>  Wed, 31 Mar 2004 21:13:50 +0200

norwegian (2.0-12) unstable; urgency=low

  * Fix aspell file names and add .multi files (closes: #192150)

 -- Tollef Fog Heen <tfheen@debian.org>  Tue,  6 May 2003 22:00:38 +0200

norwegian (2.0-11) unstable; urgency=low

  * Really add build-deps on ssed (closes: #187453)

 -- Tollef Fog Heen <tfheen@debian.org>  Fri,  4 Apr 2003 14:15:41 +0200

norwegian (2.0-10) unstable; urgency=low

  * Set encoding of templates to UTF8. (closes: #175333)
  * Add Danish templates (closes: #174732)
  * Rebuild with new aspell and bump build-deps (closes: #177924, #183215,
    #185119)
  * Actually add compat symlinks. (closes: #177908, #182468)
  * Fix name of Nynorsk to New Norwegian, not Standard Norwegian. 
  * Use ssed instead of sed, since sed has turned into the slowest piece
    of software on earth.

 -- Tollef Fog Heen <tfheen@debian.org>  Fri,  4 Apr 2003 14:15:14 +0200

norwegian (2.0-9) unstable; urgency=low

  * Add /usr/share/pspell files referring to what dictionary to
    use. (closes: #167907)
  * Use UTF8 file names.  This causes dictionary-common not to barf on the
    names.  At least not as hard.  (closes: #166885)
  * Change coding-system in info-files for ispell-common, since perl
    chokes on strings it thinks are utf8, while they are latin1.
  * Fix grammar in inorwegian's template (closes: #165064)

 -- Tollef Fog Heen <tfheen@debian.org>  Fri, 25 Oct 2002 02:02:58 +0200

norwegian (2.0-8) unstable; urgency=low

  * Update to ispell-common and friends.  (closes: #164250, #164268)

 -- Tollef Fog Heen <tfheen@debian.org>  Wed, 16 Oct 2002 01:38:13 +0200

norwegian (2.0-7) unstable; urgency=low

  * Make /usr/lib/aspell/* mode 0644.

 -- Tollef Fog Heen <tfheen@debian.org>  Sat, 27 Apr 2002 22:51:59 +0200

norwegian (2.0-6) unstable; urgency=low

  * Fix typo in patterns/Makefile (closes: #144712)

 -- Tollef Fog Heen <tfheen@debian.org>  Sat, 27 Apr 2002 13:50:15 +0200

norwegian (2.0-5) unstable; urgency=low

  * Add spanish debconf template (closes: #134455)
  * Add russian aspell-no debconf template (closes: #136581)
  * Add russian wnorwegian debconf template (closes: #137699)
  * Add russian inorwegian debconf template (closes: #137661)
  * Aspell provides aspell-dictionary (closes: #139498)

 -- Tollef Fog Heen <tfheen@debian.org>  Sat,  6 Apr 2002 22:02:15 +0200

norwegian (2.0-4) unstable; urgency=low

  * Fix inorwegian postinst (closes: #121241)

 -- Tollef Fog Heen <tfheen@debian.org>  Tue, 27 Nov 2001 18:54:44 +0100

norwegian (2.0-3) unstable; urgency=low

  * Make aspell-no as well, trick taken from how aspell-da is built.
  * Add spanish debconf translation (closes: #117684)

 -- Tollef Fog Heen <tfheen@debian.org>  Sat, 17 Nov 2001 23:03:40 +0100

norwegian (2.0-2) unstable; urgency=low

  * Add default entry in inorwegian.templates
  * Add german entries to inorwegian.templates (closes: #105863)
  * Include short words as well.
  * Update build-depends (need gawk)
  * Fix makefile to use gawk explicitly (closes: #109799)
  * Explicitly unset LANG in Makefile.  This makes join work as expected.

 -- Tollef Fog Heen <tfheen@debian.org>  Tue, 14 Aug 2001 23:52:54 +0200

norwegian (2.0-1) unstable; urgency=low

  * New upstream release
  * Added German debconf entries (closes: #98393)
  * Fix up the upstream makefile, which uses awk --source and
    --re-interval
  * Build both bokmål and nynorsk variants.  Bokmål is the one installed
    as norsk, with a symlink
  * Fix typo in wnorwegian.config (closes: #98790)
  * close NMU fixed bug (closes: #76373)

 -- Tollef Fog Heen <tfheen@debian.org>  Mon,  9 Jul 2001 19:09:18 +0200

norwegian (1.1a-9) unstable; urgency=low

  * upstream release

 -- Tollef Fog Heen <tfheen@debian.org>  Thu, 17 May 2001 11:55:27 +0200

norwegian (1.1a-8) unstable; urgency=low

  * Fix typo in wnorwegian.config.  (Thanks to heikkih for that)
  * Fix up the templates file a tiny bit and moved the update-alternatives
    to the postinst. (closes: #48354)
  * We clean up the /etc/dictionary symlink (this is done long since, but
    the bug hasn't been closed). (closes: #40323)
  * Close old NMU fixed bug. (closes: #84424)

 -- Tollef Fog Heen <tfheen@debian.org>  Sun, 13 May 2001 09:41:32 +0200

norwegian (1.1a-7) unstable; urgency=low

  * New maintainer
  * remove dh_testversion, as it is no longer needed, also, since we are
    using DH_COMAT=2, update the Build-Depends line
  * Unbreak my previous brokenness.  (closes: #91234)
  * added debconf dependency
  * added sourcing of debconf library to postinst

 -- Tollef Fog Heen <tfheen@debian.org>  Sat,  5 May 2001 17:25:45 +0200

norwegian (1.1a-6.1) unstable; urgency=low

  * Non maintainer upload
  * fix a typo in the postinst which prevented inorwegian from being found
    by update-ispell-dictionary.  Closes: #84424
  * Change architecture all -> any.  Closes: #76373
  * Updated Build-Depends.
  * Updated copyright file to point to the right place.

 -- Tollef Fog Heen <tfheen@debian.org>  Sat, 24 Mar 2001 13:03:29 +0100

norwegian (1.1a-6) unstable; urgency=low

  * Now use debconf for wnorwegian.
  * Added some cleanup code to remove old norwegian dictionary.
    Closes: #40323

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Mon,  3 Oct 1999 00:00:00 +0200

norwegian (1.1a-5) frozen unstable; urgency=low

  * Typo in description fixed
  * Lowered priority number for update-alternatives from 50 to 20.

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Mon, 19 Oct 1998 20:34:55 +0200

norwegian (1.1a-4) frozen unstable; urgency=low

  * Now uses update-alternatives for choosing a dictionary, thanks to
    Charles Briscoe-Smith <cpbs@debian.org>. closes: BUG#27953

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Sun, 18 Oct 1998 15:34:17 +0200

norwegian (1.1a-3) unstable; urgency=low

  * Changed the filename of the dictionary from "norwegian" to "norsk"
  * Moved dictionary to /usr/share/dict instead of /usr/dict 

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Fri,  9 Oct 1998 22:21:18 +0200

norwegian (1.1a-2) unstable; urgency=low

  * Used lintian to remove buglets and annoyances
  * Closed bugs #3688, #3912, #6364, #9398, #9540

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Wed, 29 Jul 1998 19:51:43 +0200

norwegian (1.1a-1) unstable; urgency=low

  * Initial Release.

 -- Stig Sandbeck Mathisen <ssm@debian.org>  Fri, 12 Jun 1998 00:38:53 +0200
