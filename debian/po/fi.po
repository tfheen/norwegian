msgid ""
msgstr ""
"Project-Id-Version: norwegian_2.0.9-1\n"
"Report-Msgid-Bugs-To: tfheen@debian.org\n"
"POT-Creation-Date: 2007-07-18 20:02+0200\n"
"PO-Revision-Date: 2007-12-22 12:21+0200\n"
"Last-Translator: Esko Arajärvi <edu@iki.fi>\n"
"Language-Team: Finnish <debian-l10n-finnish@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Finnish\n"
"X-Poedit-Country: Finland\n"

#. Type: select
#. Choices
#. bokmaal is an ASCII transliteration. aa is in fact the <U00C5>
#. Unicode glyph, rendered as a lower a with a ring above.  Please
#. use this glyph when available.  The HTML entity is &aring;
#: ../wnorwegian.templates.in:2001 ../inorwegian.templates.in:2001
#: ../aspell-no.templates:2001
msgid "nynorsk"
msgstr "uusnorja"

#. Type: select
#. Choices
#. bokmaal is an ASCII transliteration. aa is in fact the <U00C5>
#. Unicode glyph, rendered as a lower a with a ring above.  Please
#. use this glyph when available.  The HTML entity is &aring;
#: ../wnorwegian.templates.in:2001 ../inorwegian.templates.in:2001
#: ../aspell-no.templates:2001
msgid "bokmaal"
msgstr "kirjanorja"

#. Type: select
#. Description
#: ../wnorwegian.templates.in:2002 ../inorwegian.templates.in:2002
#: ../aspell-no.templates:2002
msgid "Norwegian language variant:"
msgstr "Norjan kielen muunnelma:"

#. Type: select
#. Description
#: ../wnorwegian.templates.in:2002 ../inorwegian.templates.in:2002
#: ../aspell-no.templates:2002
msgid "Norwegian has two different written forms: bokmaal and nynorsk."
msgstr "Norjan kirjakielestä on kaksi eri versiota: kirjanorja ja uusnorja."

#. Type: select
#. Description
#: ../wnorwegian.templates.in:2002 ../inorwegian.templates.in:2002
#: ../aspell-no.templates:2002
msgid "Please choose the one you wish to use."
msgstr "Valitse kumpaa haluat käyttää."

#~ msgid "Which variant?"
#~ msgstr "Kumpi muunnelma?"

#~ msgid "Which one do you want to use?"
#~ msgstr "Kumpaa haluat käyttää?"

#~ msgid ""
#~ "As you might know, Norwegian has two different written forms, bokmaal and "
#~ "nynorsk."
#~ msgstr ""
#~ "Kuten saatat tietää, norjan kirjakielestä on kaksi versiota: kirjanorja "
#~ "ja uusnorja."

#~ msgid "Which one do you want ispell to use as Norwegian?"
#~ msgstr "Kumpaa haluat ispellin käyttävän norjan kielenä?"

#~ msgid "Which one do you want aspell to use as Norwegian?"
#~ msgstr "Kumpaa haluat aspellin käyttävän norjan kielenä?"
