Changes in the Norwegian spell checking package

Release 2.2 (2016-04-15)

 * Rewrite how scripts/speling2words handle tripple consonants, to
   avoid importing duplicate words from no.speling.org, and getting
   rid of the existing duplicates in norsk.words.
 * Remove duplicate entries with tripple consonants from norsk.words.
 * Update frequency for entries in norsk.words based on
   <URL:http://helmer.aksis.uib.no/nta/ordlistf.zip> (ran 'make
   freq-update').
 * Correct nn ispell build, avoid crash in munchlist causing lots of
   words to fall out of the database.
 * Use grep -a to convince grep it is working on text files, to work
   with newer grep versions.

 * Remove some words disputed in the no.speling.org review process:
    - apparent (nb)
    - likke (nb)
    - ugjest, ugjesten, ugjestens (nb)

Release 2.1 (2012-09-30)

 * Switch to new version scheme. Make new version 2.1, not 2.0.11.  We
   do not release often enough to justify three digits.
 * Switch build rules to build OOo v2 thesaurus files, as the v1 build
   rules no longer work.  This require the libmythes-dev package on
   Debian.
 * Introduce new Makefile variables hyphendir and thesdir to make it
   easier to control where to install these.
 * Change script used to import from no.speling.org, to load new word
   boundaries if at least two people believed the boundaries was
   correct.
 * Added word boundaries for several words (around 500 words) using
   the updated script.
 * Imported thesarus for bokm�l from synonymer.merg.net.
 * Rewrote build rules to use = instead of - as combined word marker, thus
   allowing words like e-post.
 * Imported a lot (around 10k words) of new words with dash (-) in
   them from no.speling.org now that it is handled by the build
   system.

Release 2.0.10 (2008-03-10)

 * Added . after every synonym and put them all into synonymer-nb.txt and 
   not in norsk.words, from where they are removed.
   --Moved: bm, bnr, cand, dvs, etc, ev, mag, osv, pga
   --Changed: 'eg * 21 B.  to  'eg *' (to only use it in nynorsk)
              'mm *' to 'mm * 0 B' (to use it also in bokm�l)
   --Removed mao and phil
   --Removed from forkort-nb.txt: 
     Ph.D., S.u., a.a., a.a.C.a., a.m., c.c., cand.occon., cand.rer.polit., 
     f.o., fr.o.m., f�r., f�re., h.o.h., h.t., i.l., k.o., m.a., mu.h., 
     o.dyl., pr.pr., r.p., res.kap., s.st., s.�., sq.in., stud.rer.polit., 
     v.hj.a., z.B.,
   --Added in forkort-nb.txt:
     A.C., KrF., Pb., Ph.D., S.s.v., S.u., a.a.C.n., adm., adr., am., bill., 
     bl., bm., cand.hort., cand.oecon., cand.polit. cand.san., cand.scient., 
     cand.sociol, dir., disp., div., dr., dr.art., dr.oecon., dr.scient.,
     dvs., eg., el., etc., ev., fil., fm., forb., forf., fork., forl., forr., 
     forts., fr., gl., gram., hoh., ill., jr., kl., kr., lat., laud., lev., 
     lign., likn., litt., l�., ma., mag., maks., mat., mat.nat., mfl., muh., 
     mus., mva., nat., nr., n�dv., obj., obl., obs., omarb., omg., omtr., on., 
     oppr., org., orig., osv., pga., poet., pol., pr., priv., prod., prof., 
     pron., prot., psyk., pt., q.v., rel., s.�., siviling., sms., sos., 
     sos.dem., sovj., sst., st., stip., stud., stud.san., subj., subst., s�k., 
     s�., tekn., teol., ti., tlf., to., ub., ubest., ug., univ., utg., utt., 
     uttr., v.v., vgs., vha., vs., �rg., �rh., �rl., �v., �.l., �kol., �kon ., 

 * Add aspell-phonect.dat from Olaf Havnes.  Created initial install rules
   for it.  The install rules need to be checked.
 * Added forms rykker-brevs, rykker-brevene, rykker-brevenes, and marked
   all forms of this word as K (conservative) (nb)
 * Added word separator for all words containing Inter-nett.  Removed
   dupliate entry for 'Internett'.  Did similar for av-montere,
   av-montering, frem-over, f�re-setnad, inne-held, inne-halde.
 * Removed dupliate entry for av-leie, ignorerande, nettverks-tenesta,
   nettverks-teneste, nettverks-tenestene, nettverks-tenester,
   rose-maler, skole-medisiner, stor-spiller, tjukkas.
 * Imported thesarus for bokm�l from synonymer.merg.net.
 * Add rules to build thesarus for OOo v2.
 * Updated dictionary server used by the bokmaal script from
   www.dokpro-test.uio.no to www.dokpro.uio.no.
 * Added 623500 new words from no.speling.org, most of them imported
   to no.speling.org from the norwegian project ordbanken.

Release 2.0.9 (2007-02-19)

 * Updated nb frequency information for words with zero as their
   frequency value based on info received from Kevin Patrick Scannell
   and the An Cr�bad�n project.
 * Add two make targets speling-new.nb and speling-new.nn to make
   lists of new words based on the no.speling.org database.
 * Updated the nb and nn thesaurus from the no.speling.org system.
 * Correct handling of " when making aspell dictionary.
 * Add combined word marker (-) for words including s�r-kull.
 * Rename forkort.txt to forkort-nb.txt to make it more obvious how to
   make a similar file for nn.  Created forkort-nn.txt and adjusted
   build rules to use it.
 * New words from the no.speling.org database:
   - CD (nb,nn)
   - CDane (nn)
   - CDar (nn)
   - DHCP (nb,nn)
   - DHCP-vertsnamn (nn)
   - Internett (nb,nn)
   - LP (nb,nn)
   - angrast (nn)
   - annen (nn)
   - arv-tager (nb)
   - automat-pistol (nb)
   - avleia (nn)
   - avleie (nn)
   - avmontere (nb,nn)
   - avmonterer (nb,nn)
   - avmontering (nb,nn)
   - avslutt (nn)
   - bilde-samling (nb)
   - bygge (nn)
   - byt (nn)
   - defekte (nb)
   - dela (nn)
   - drifts-huset (nb)
   - drivar (nn)
   - drivar-diskett (nn)
   - drivar-diskettar (nn)
   - drivar-disketten (nn)
   - drivarane (nn)
   - drivarar (nn)
   - drivaren (nn)
   - dyre-elskere (nb)
   - d�mer (nn)
   - empati (nb)
   - est (nb)
   - etanol (nb)
   - fag-felle-vurdert (nb,nn), fag-felle-vurdering (nb,nn)
   - ferie-bilen (nb)
   - fil-nedlasting (nb,nn)
   - fil-nummer (nn)
   - fil-nummeret (nn)
   - fil-omr�da (nn)
   - fil-omr�de (nn)
   - fil-omr�der (nn)
   - fil-omr�det (nn)
   - fil-system (nn)
   - fil-system-blokk (nb,nn)
   - fil-system-blokka (nn)
   - fil-system-blokkene (nb,nn)
   - fil-system-parameter (nb,nn)
   - fil-system-tre (nb,nn)
   - fil-system-treet (nb,nn)
   - fil-system-type (nb,nn)
   - fil-system-typen (nb,nn)
   - fil-systema (nn)
   - fil-systemer (nn)
   - fil-systemet (nn)
   - fil-tilgang (nn)
   - fil-tilgangen (nn)
   - finans-departement (nb), finans-departementet (nb)
   - finna (nn)
   - flikk-arbeid (nb)
   - formatera (nn)
   - forsknings-departementet (nb)
   - fritere (nb)
   - fungera (nn)
   - f�rehands-oppsetts-fila (nn)
   - f�resetnad (nn)
   - f�resetnadane (nn)
   - f�resetnadar (nn)
   - f�resetnaden (nn)
   - gjentakande (nn)
   - gjentake (nn)
   - greit (nn)
   - halda (nn)
   - haldar (nn)
   - haldarar (nn)
   - haldaren (nn)
   - halo (nb)
   - handterar (nn)
   - handterarane (nn)
   - handterarar (nn)
   - handteraren (nn)
   - hopp-mesterskap (nb), hopp-mesterskapet (nb)
   - hovud-distribusjon (nn)
   - hovud-distribusjonar (nn)
   - hovud-distribusjonen (nn)
   - hovud-oppsett (nn)
   - hovud-oppsetta (nn)
   - hovud-oppsettet (nn)
   - hovud-oppsetts-fil (nn)
   - hovud-oppsetts-fila (nn)
   - hovud-oppsetts-filene (nn)
   - hovud-oppsetts-filer (nn)
   - hovud-oppstarts-spor (nn)
   - hovud-oppstarts-spora (nn)
   - hovud-oppstarts-sporet (nn)
   - hus-bil (nb), hus-bilen (nb), hus-bilene (nb)
   - hukre (nb)
   - hvile-stedene (nb)
   - idretts-skyttere (nb)
   - ignorer (nn)
   - ignorerande (nn)
   - inert-gass (nb)
   - inert-gassen (nb)
   - inert-gassene (nb)
   - inert-gassenes (nb)
   - inert-gassens (nb)
   - inert-gasser (nb)
   - innehalda (nn)
   - innehalde (nn)
   - innehaldt (nn)
   - inneheld (nn)
   - innkj�ps-ansvarlig (nb)
   - innovativt (nb)
   - innskudds-kontoer (nb)
   - installasjons-CD (nb,nn)
   - installasjons-CDar (nn)
   - installasjons-CDen (nb,nn)
   - installasjons-CDer (nb)
   - installera (nn)
   - installerbar (nb,nn)
   - installerbare (nb,nn)
   - investerings-foretaka (nb), investerings-foretakas (nb)
   - ip-adresse (nb,nn)
   - jemtlending (nb)
   - j�vel (nb)
   - kammer-musikk-arrangementet (nb)
   - kjerna (nn)
   - kjerne-modul-pakka (nb,nn)
   - kjerne-modul-pakkar (nn)
   - kjerne-modul-pakke (nb,nn)
   - kjerne-modul-pakker (nb,nn)
   - kj�pe-videoene (nb), kj�pe-videoenes (nb), kj�pe-videoer (nb), kj�pe-videoers (nb), kj�pe-videos (nb)
   - kontroll-tids-punkt (nb), kontroll-tids-punktet (nb)
   - kontrollera (nn)
   - koordinators (nb)
   - kopier (nn)
   - kopiera (nn)
   - kring-kastings-nettene (nb)
   - kvart�rs (nn)
   - leggja (nn)
   - leggjast (nn)
   - leggje (nn)
   - lesa (nn)
   - ligge (nn)
   - linux (nb,nn)
   - lisens-kostnad (nb), lisens-kostnaden (nb), lisens-kostnadene (nb), lisens-kostnader (nb)
   - lyn-melding (nb), lyn-meldingen (nb), lyn-meldingene (nb), lyn-meldinger (nb)
   - l�s-fart (nb)
   - l�ysa (nn)
   - mega (nn)
   - mega-byte (nn)
   - mega-hertz (nn)
   - mega-konsert (nn)
   - mega-konserten (nn)
   - mellom-lager (nn)
   - mellom-lageret (nn)
   - meter-bylgja (nn)
   - meter-bylgje (nn)
   - meter-bylgjene (nn)
   - meter-bylgjer (nn)
   - mikrokode-niv� (nb,nn)
   - mikrokode-niv�a (nb,nn)
   - mikrokode-niv�ene (nb)
   - mikrokode-niv�et (nb,nn)
   - milit�r-drakt (nb)
   - montera (nn)
   - multidisk-eining (nn), multidisk-eininga (nn), multidisk-einingane (nn), multidisk-einingar (nn)
   - m�lbarhet (nb)
   - ned-rustnings-meldinga (nb)
   - nede-tid (nb), nede-tiden (nb)
   - nettenar (nn), nettenarane (nn), nettenarar (nn), nettenaren (nn)
   - nettverks-konsoll (nb,nn)
   - nettverks-konsollane (nn)
   - nettverks-konsollar (nn)
   - nettverks-konsollen (nb,nn)
   - nettverks-konsollene (nb)
   - nettverks-konsoller (nb)
   - nettverks-tenesta (nn)
   - nettverks-teneste (nn)
   - nettverks-tenestene (nn)
   - nettverks-tenester (nn)
   - notasjon (nn), notasjonane (nn), notasjonar (nn), notasjonen (nn)
   - n�kkel-ID (nb,nn)
   - n�kkel-IDane (nn)
   - n�kkel-IDar (nn)
   - n�kkel-IDen (nb,nn)
   - n�kkel-IDene (nb)
   - n�kkel-IDer (nb)
   - oppdater (nn)
   - opprinnelses-land (nb)
   - oppsto (nn)
   - parti-sammenhengen (nb), parti-sammenhengene (nb), parti-sammenhengenes (nb), parti-sammenhengens (nb), parti-sammenhenger (nb), parti-sammenhengers (nb), parti-sammenhengs (nb)
   - pass� (nb)
   - penge-sjefen (nb)
   - penge-tap (nb)
   - pris-redusert (nb), pris-reduserte (nb)
   - produkt-ansvarlig (nb)
   - propriet�re (nb)
   - p�-kravd (nn), p�-kravde (nn)
   - redusera (nn)
   - reise-bilen (nb)
   - respons-tider (nb)
   - sammen-filtring (nb)
   - senda (nn)
   - setja (nn)
   - siffera (nn)
   - sjekksum-fil (nb,nn), sjekksum-fila (nb,nn)
   - ski-anlegg (nb), ski-anlegget (nb)
   - slettast (nn)
   - sokalla (nn)
   - steg-vis (nb)
   - str�m-kundenes (nb), str�m-kunders (nb), str�m-kundes (nb)
   - suge-kamrene (nb)
   - skjedenes (nb), skjeders (nb), skjedes (nb)
   - t.d. (nn)
   - test-program (nb)
   - trenga (nn)
   - tross (nn)
   - trykkja (nn)
   - ulvinne (nb)
   - uoppretta (nn)
   - ut-rulling (nb)
   - utdata (nn)
   - utkommentere (nb,nn), utkommenterer (nb,nn), utkommenterter (nb,nn)
   - utl�ns-virksomheta (nb), utl�ns-virksomhetas (nb), utl�ns-virksomhetene (nb), utl�ns-virksomhetenes (nb), utl�ns-virksomheter (nb), utl�ns-virksomheters (nb), utl�ns-virksomhets (nb)
   - utstr�la (nn)
   - variera (nn)
   - vei-viserne (nb)
   - ven (nb)
   - verdens-makta (nb)
   - verdens-systemet (nb)
   - verifisera (nn)
   - versjons-kontroll (nb)
   - vinsjenes (nb), vinsjers (nb), vinsjs (nb)
   - �dsleriet (nb)
   - �ydeleggja (nn)

 * Removed words
   - leggte (nn)
   - leggtja (nn)
   - leggtjst (nn)

Release 2.0.8 (2006-06-30)

 * Found out how to make the OOo-wizard in OOo 1.x find the thesaurus 
   files for nynorsk and bokm�l and install them properly. 
   OOo 2.0 will still only find the bokm�l package, because this is a 
   separate file in another format.
 * Made some minor improvements on the documentation files.

Release 2.0.7 (2006-06-30)

 * Added a beta version of a bokm�l thesarus for OOo 1.x and 2.x,
   downloaded from <synonymer.merg.net>.
 * Translated the README-files for the thesarus-packages to norwegian.
 * Added a very small nn thesaurus generated with data from the
   no.speling.org system (just a alfa version, mostly for testing :-).
 * Removed filter in Makefile for the source file from no.speling.org,
   as the errors are fixed upstream.
 * Updated the nb thesaurus from the no.speling.org system.
 * Lower the nb frequency cutoff point from >0 to >=0, to get even
   more words included in the spell check systems for nb.  This
   increases the number of nb from 352055 to 545714.  I believe this
   will include all nb words.

 * New words:
   - m�le-l�p (nb,nn), m�le-l�pa (nn), m�le-l�pene (nb), m�le-l�penes (nb),
     m�le-l�pet (nn), m�le-l�pets (nb, nn).  Frequence 0 as none of
     these words are listed in the word frequency lists we have access
     to.  Thanks to Jens Blix for reporting this missing word.
   - i (nb,nn).  Frequence set to 31 as it is a very common word.
     Thanks to Lars Oftedal for discovering this missing word.
   - �kse-morder (nb), �kse-mordere (nb), �kse-morderen (nb),
     �kse-morderens (nb), �kse-morderes (nb), �kse-morderne (nb),
     �kse-mordernes (nb), �kse-morders (nb) �kse-mordene (nb),
     �kse-mordenes (nb).  Freq 0 until updated with real info.  Thanks
     to Lars Oftedal for discovering that �ksemorder was missing.

 * Changed words:
   - Allow �kse-mord and �kse-mordet for nb as well.  Freq 0 for now.
   - Made '�' into a valid nb word (as well as the existing nn
     setting), and set the frequency info to 31 as it is a common
     word.

Release 2.0.6 (2006-02-24)

 * Update the nb thesaurus from the no.speling.org system.
 * Rewrite myspell and OOo install rule for thesaurus to use 'th_'
   prefix on the files.

Release 2.0.5 (2006-02-19)

 * Drop radical samnorsk 'S' class words from the nb dictionary.
 * Update nohyphb.tex with the improved version nohyphbx.tex from
   <URL:http://home.c2i.net/omselberg/pub/nohyphbx_intro.htm>.
 * Update the new nohyphb.tex to handle 8bit chars consistently, and
   make it easier to transform automatically.
 * Add build rule to generate OOo-formattet hyphernation format from
   nohyphb.tex.  Include this generated file in the OOo-package
   instead of the files in ooo-hyph/.
 * Update the thesaurus from the no.speling.org system.

Release 2.0.4 (2006-02-16)

 * Generate the thesaurus from the no.speling.org system, instead
   of maintaining the list manually.  Only include non-controversial
   words.  All the prevoious synonyms are now available from there.
 * Rewrite myspell build to use the munched words from the ispell
   build, to reduce the build time.
 * Added build rule 'ooo-dist' to make a OpenOffice.org spellcheck
   package.
 * New words:
   - sol-ur (nb,nn), sol-urene (nb,nn), sol-uret (nb,nn), sol-urets (nb,nn)

Release 2.0.3 (2006-01-15)

 * Update character frequency information used by myspell, and copy a
   few AFF lines from the OOo spell-checking package.
 * Updated nb frequency information for words with zero as their
   frequency value based on info received from Kevin Patrick Scannell
   and the An Cr�bad�n project.  This added 583 words to the nb spell
   checker.
 * Rewrite myspell build rules to use the tools from the myspell
   package. Now uses 'munch' the full list of nb and nn words to
   generate the dictionary files and 'ispellaff2myspell' from the
   myspell package instead of the home grown 'iaff2myaff.pl' to
   convert the affix file to myspell format.
 * Correct aspell build rules to make sure the {nb,nn}.dat files are
   available.
 * Try to optimize the ispell build rules by moving more filtering
   into sed.
 * Use makefile variables for most word separator filtering, to make
   it easier to switch separator character in the future.
 * Added several new words in the nb thesaurus extracted from the free
   nb word database. being compiled on <URL:http://no.speling.org/>.
 * Comment out altstringchar rules for iso246 in nb.aff.in and
   nn.aff.in, as it confuses munchfile in ispell 3.3.02.

 * New words
   - Alexandras (nb,nn).
   - regional-departement (nb), regional-departementet (nb) and
     regional-departementets (nb).
   - fast-lege (nb,nn), fast-legen (nb,nn), fast-legens (nb).

Release 2.0.2 (2006-01-04)

 * Corrected myspell dict file count line.
 * Made it easier to replace 'echo -e' for platforms where -e is not a
   valid option to echo.
 * Added 'install-doc' target to install documentation files.
 * Install ispell dictionaries using 'nb' and 'nn' names, and make
   symlinks to these from the old names.
 * Add script and make rule 'freq-update' to update the frequency
   information based on data from NTA, <URL:http://helmer.aksis.uib.no/nta/>.

 * New words:
   - fremover (nb).
   - Internet (nb,nn).
   - internettet (nb).
   - s�r-emne (nb).
   - vassdrags-tiltak (nb).

 * Changed words:
   - Updated lots of words with freq 0 to the freq value provided from
     NTA.  This added 9787 words to the nb list.
   - Update frequency information for all new words in 2.0.1.  Set to
     '1' for words not available from NTA, to make sure they are
     included in the nb dictionary.

Release 2.0.1 (2005-12-31)

 * Now being group maintained on Alioth.
 * Updated package to use new email address for Rune Kleveland.
 * Rewrote build rules based on Debian patches, to make it easier to
   make binary packages based on this source.
 * Rewrite build rules to use the language codes 'nb' and 'nn'
   instead of 'norsk' and 'nynorsk'.
 * Added build rules for aspell and myspell, based on the rules
   in the debian package.
 * Started on myspell (OOo) thesaurus files for bokm�l (nb).
 * Added new script 'bokmaal', capable of looking up words on the web
   service available from <URL:http://www.dokpro.uio.no/>.
 * Lower the nb frequency cutoff point from >9 to >0, to get more
   words included in the spell check systems for nb.

 * New words:
    - DVD (nb,nn).
    - fil-rettighet (nb), fil-rettigheten (nb), fil-rettigheter (nb)
    - ignoranse (nb).
    - Internett (nb,nn), internett-* (nb,nn).
    - internett-leverand�r (nb,nn), internett-leverand�ren (nb),
      internett-leverand�rer (nb).
    - kontrakts-forslag (nb), kontrakts-forslaget (nb).
    - krypto (nb).
    - navne-tjener (nb), navne-tjenere (nb), navne-tjeneren (nb).
    - Reinholdtsen (nb,nn).
    - sikkerhets-oppdatering (nb), sikkerhets-oppdateringen (nb),
      sikkerhets-oppdateringene (nb).
    - Skolelinux (nb), Skulelinux (nn).
    - Skolelinuxprosjektet (nb), Skulelinuxprosjektet (nn).
 * Changed words:
    - Internett-adressene: freq ""->2, to make it visible as a nb word.
    - Linux: freq 0->2
    - Linux-*: freq 0->2
    - rekursiv: req 0->2

Release 2.0 (2000-09-12)

 [ NEWS file did not exist then ]

Release 1.1a (1998-07-06)

 [ NEWS file did not exist then ]

Release 1.1 (1998-06-03)

 [ NEWS file did not exist then ]

Release 1.0 (1998-05-20)

 [ NEWS file did not exist then ]
